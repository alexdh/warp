<?xml version="1.0" encoding="UTF-8"?>
<!-- Fina Wilke 2022 <code@felinira.net> -->
<component type="desktop-application">
  <id>@app-id@</id>
  <metadata_license>CC0</metadata_license>
  <project_license>@license@</project_license>
  <name>Warp</name>
  <summary>Fast and secure file transfer</summary>
  <description>
    <p>
      Warp allows you to securely send files to each other via the internet or local network by
      exchanging a word-based code.
    </p>
    <p>
      The best transfer method will be determined using the “Magic Wormhole” protocol which includes
      local network transfer if possible.
    </p>
    <p>Features</p>
    <ul>
      <li>Send files between multiple devices</li>
      <li>Every file transfer is encrypted</li>
      <li>Directly transfer files on the local network if possible</li>
      <li>An internet connection is required</li>
      <li>QR Code support</li>
      <li>Compatibility with the Magic Wormhole command line client and all other compatible apps</li>
    </ul>
  </description>
  <screenshots>
    <screenshot>
      <image>https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot1.png</image>
      <caption>Main Window</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot2.png</image>
      <caption>Transmit Code</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot3.png</image>
      <caption>Accept File Transfer</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot4.png</image>
      <caption>Sending File</caption>
    </screenshot>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot5.png</image>
      <caption>Receiving File</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot6.png</image>
      <caption>File Transfer Complete</caption>
    </screenshot>
  </screenshots>
  <provides>
    <binary>warp</binary>
  </provides>
  <url type="homepage">@homepage@</url>
  <url type="bugtracker">@issuetracker@</url>
  <url type="contact">@contact@</url>
  <custom>
    <value key="GnomeSoftware::key-colors">[(136, 101, 247), (198, 130, 255)]</value>
    <value key="Purism::form_factor">workstation</value>
    <value key="Purism::form_factor">mobile</value>
  </custom>
  <content_rating type="oars-1.0" />
  <releases>
    <release version="0.5.2" date="2023-04-12">
      <description>
        <p>This release includes the following features:</p>
        <ul>
          <li>Fix a rare issue where an error message would be “stuck”</li>
          <li>Add progress info to zip file creation</li>
        </ul>
      </description>
    </release>
    <release version="0.5.1" date="2023-03-22">
      <description>
        <p>Specify correct dependency versions in meson</p>
      </description>
    </release>
    <release version="0.5.0" date="2023-03-22">
      <description>
        <p>This release includes the following features:</p>
        <ul>
          <li>Update runtime to GNOME 44</li>
          <li>Fix “Show in Folder” for Dolphin users</li>
          <li>Add Shortcuts and tweaks to align the experience with other GNOME apps</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" date="2023-02-01">
      <description>
        <p>This release includes the following features:</p>
        <ul>
          <li>QR Code support</li>
          <li>Save files to other destinations apart from the downloads folder</li>
          <li>Experimental Windows support. You can a download nightly build to try it out.</li>
          <li>Seasonal pride progress bars</li>
          <li>Important bug fixes</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="0.3.2" date="2022-12-11">
      <description>
        <p>Bugfix release</p>
        <ul>
          <li>Fix Progress bar would not move sometimes</li>
          <li>Fix Transfer cancellation issues</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="0.3.1" date="2022-10-24">
      <description>
        <p>Bugfix release</p>
        <ul>
          <li>Fix about window</li>
          <li>File received notification now opens the containing folder when clicked</li>
          <li>Update to gtk-rs 0.5</li>
          <li>Enable drag and drop for flatpak. Confirmed to work with Nautilus 43 and gtk 4.8.1
            upwards</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" date="2022-09-23">
      <description>
        <p>Update for GNOME 43</p>
        <ul>
          <li>Rework multiple UI elements</li>
          <li>Use libadwaita 1.2</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="0.2.4" date="2022-08-27">
      <description>
        <p>Fixes an issue with the code entry disappearing after a prior failed transfer</p>
      </description>
    </release>
    <release version="0.2.3" date="2022-08-09">
      <description>
        <p>This patch release has the following changes:</p>
        <ul>
          <li>Fixes issue with 'Select Folder' not working correctly</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="0.2.2" date="2022-07-13">
      <description>
        <p>Translation updates and minor bug fixes</p>
      </description>
    </release>
    <release version="0.2.1" date="2022-06-28">
      <description>
        <p>This patch release has the following changes:</p>
        <ul>
          <li>Inhibit computer sleep while transfer is running</li>
          <li>Button to open the download folder after transfer is complete</li>
          <li>Change URI format to use the `wormhole-transfer` specification for future
            interoperability with other magic-wormhole clients</li>
          <li>Translation updates</li>
          <li>Metadata improvements</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" date="2022-06-17">
      <description>
        <p>This release includes the following updates:</p>
        <ul>
          <li>Design improvements</li>
          <li>Many new translations</li>
          <li>Support for mobile devices</li>
          <li>Improved error handling</li>
          <li>Improved temporary file handling</li>
          <li>Add preferences screen for server URLs</li>
        </ul>
      </description>
    </release>
    <release version="0.1.2" date="2022-05-17">
      <description>
        <p>Added French translation</p>
      </description>
    </release>
    <release version="0.1.1" date="2022-05-12">
      <description>
        <p>Metadata updates and bugfixes</p>
      </description>
    </release>
    <release version="0.1.0" date="2022-05-10">
      <description>
        <p>Initial release</p>
      </description>
    </release>
  </releases>
  <kudos>
    <!--
       GNOME Software kudos:
    https://gitlab.gnome.org/GNOME/gnome-software/blob/main/doc/kudos.md
     -->
    <kudo>ModernToolkit</kudo>
    <kudo>HiDpiIcon</kudo>
    <kudo>HighContrast</kudo>
  </kudos>
  <developer_name>Fina Wilke</developer_name>
  <update_contact>code@felinira.net</update_contact>
  <translation type="gettext">@gettext-package@</translation>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <recommends>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
  </recommends>
</component>